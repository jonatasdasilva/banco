#!/usr/bin/python3
# -*- coding: utf-8 -*-

import csv

import json
from pymongo import MongoClient

# Conexão MongoDB
host = 'localhost'
port = 27017

cliente = MongoClient(host, port)
print("Conexao mongo feita")
database = cliente.nypd
print("database selecionado")
collisions = database.collisions
print("collection selecionada")

# Variaveis locais para os documentos JSON
data = {}
local = {}
factor = {}
person = {}
vehicle = {}
cyclist = {}
motorist = {}
pedestrians = {}

#Funções para construção dos documentos JSON
def createData (cabecalho, dado):
    data[cabecalho] = dado

def createLocation (cabecalho, dado):
    local[cabecalho] = dado

def createPerson (cabecalho, dado):
    person[cabecalho] = dado

def createCyclist (cabecalho, dado):
    cyclist[cabecalho] = dado

def createPedestrians (cabecalho, dado):
    pedestrians[cabecalho] = dado

def createMotorist (cabecalho, dado):
    motorist[cabecalho] = dado

def createVehicle (cabecalho, dado):
    vehicle[cabecalho] = dado

def createFactor (cabecalho, dado):
    factor[cabecalho] = dado

# Insere o conteúdo do arquivo nos locais devidos
def createDocument (cabecalho, dado):
    createCyclist(cabecalho[14], dado[14])
    createCyclist(cabecalho[15], dado[15])
    createMotorist(cabecalho[16], dado[16])
    createMotorist(cabecalho[17], dado[17])
    createPedestrians(cabecalho[12], dado[12])
    createPedestrians(cabecalho[13], dado[13])
    createPerson(cabecalho[10], dado[10])
    createPerson(cabecalho[11], dado[11])
    createPerson("CYCLISTS", cyclist)
    createPerson("MOTORISTS", motorist)
    createPerson("PEDESTRIANS", pedestrians)
    createFactor(cabecalho[18], dado[18])
    createFactor(cabecalho[19], dado[19])
    createFactor(cabecalho[20], dado[20])
    createFactor(cabecalho[21], dado[21])
    createFactor(cabecalho[22], dado[22])
    createVehicle(cabecalho[24], dado[24])
    createVehicle(cabecalho[25], dado[25])
    createVehicle(cabecalho[26], dado[26])
    createVehicle(cabecalho[27], dado[27])
    createVehicle(cabecalho[28], dado[28])
    createLocation(cabecalho[2], dado[2])
    createLocation(cabecalho[3], dado[3])
    createLocation(cabecalho[4], dado[4])
    createLocation(cabecalho[5], dado[5])
    createLocation(cabecalho[6], dado[6])
    createLocation(cabecalho[7], dado[7])
    createLocation(cabecalho[8], dado[8])
    createLocation(cabecalho[9], dado[9])
    createData("_id", dado[23])
    createData(cabecalho[0], dado[0])
    createData(cabecalho[1], dado[1])
    createData("LOCAL", local)
    createData("PERSONS", person)
    createData("FACTORS", factor)
    createData("VEHICLES", vehicle)
# Grava os dados no documento JSON. Opcional, atualmente desnecessário
def writeDocument ():
    with open('data.json', 'w') as file:
        json.dump(data, file)

def insertMongo ():
    result = collisions.insert_one(data)
    print(result)
'''
def desconectAll ():
    cliente.close()

def readCSV (cabecalho, conteudo):
    with open('base.csv', 'r', encoding='utf8') as file:
        dados = csv.reader(file, delimiter=',')
        # Leituras das linhas e colunas do arquivo CSV.
        flag = False
        print(flag)
        for linhas in dados:
            for j, row in enumerate(linhas):
                #print(j, row)
                if flag:
                    conteudo.insert(j, row)
                    if j == 28 and flag:
                        print("Criando Documen")
                        createDocument(cabecalho, conteudo)
                        print("Documen criado, escrevendo")
                        writeDocument()
                        print("inserindo no banco")
                        insertMongo()
                        print("Limpando o conteudo de 'conteudo'")
                        conteudo.clear()
                else:
                    #print("inseriu no cabeçalho")
                    cabecalho.insert(j, row)
                    if row == "VEHICLE TYPE CODE 5":
                        flag = True
                # Após ler todos os dados grava no arquivo e insere no banco.
                
        file.close()
'''
def ler (cabecalho):
    with open('base.csv', 'r', encoding='utf8') as file:
        dados = csv.reader(file, delimiter=',')
        # Leituras das linhas e colunas do arquivo CSV.
        flag = False
        for linhas in dados:
            for j, row in enumerate(linhas):
                if flag:
                    break
                else:
                    cabecalho.insert(j, row)
                    if row == "VEHICLE TYPE CODE 5":
                        flag = True
            if flag:
                break
    print(cabecalho)

if __name__ == "__main__":
    #parameters = input("Insira a ação DDL: ")
    cabecalho = []
    conteudo = []
    '''
    #conectMongo()
    print("Leitura do csv...")
    #readCSV(cabecalho, conteudo)
    print("Lendo find...")
    doc = collisions.find_one({"_id": "85174"})
    print(doc)
    #pagina 34 do livro 
    doc = collisions.find({"VEHICLES.VEHICLE TYPE CODE 1": "PASSENGER VEHICLE"}).count()
    print(doc)
    cliente.close()
    #with open('data.json', 'w') as file:
    #    json.dump(data, file)

    #cliente = MongoClient(host, port)
    #database = cliente.nypd_motor
    #collisions = database.collisions
    '''
    #print("  [ DIGITE OS VALORES OU USE UM DOCUMENTO JÁ FORMATODO 'JSON' ])
    #resposta = input("  [ DESEJA DIGITAS OS VALORES? (sim ou não) ] =>> ")
    #if (resposta == "sim"):
    #arq = open('cabecalho.txt', 'r', encoding='utf-8')
    #cabecalho = arq.read()
        #cabecalho = file.read()
    #print("  [",cabecalho,"]")
    ler(cabecalho)
        #for j, campo in enumerate(cabecalho): 
        #    retorno = input("  [",campo,"] =>> ")
        #    dado.insert(j, campo)